#!/usr/bin/env bash

SCRIPT_DIR=$( cd -- "$( dirname -- "${BASH_SOURCE[0]}" )" &> /dev/null && pwd )

export site=lho
export ifo=h1

export RTS_VERSION=librts-test

export USERAPPS_BASE_PATH=/opt/rtcds/trunk/
source $USERAPPS_BASE_PATH/etc/userapps-user-env.sh #Load env for .c files included in production .mdls
export RCG_LIB_PATH=$USERAPPS_LIB_PATH
#Prepend models dir so we can overwrite model versions
export RCG_LIB_PATH=$SCRIPT_DIR/models/:$RCG_LIB_PATH 

export RCG_BUILDD=$SCRIPT_DIR/rcg-build/
export RCG_TARGET=$SCRIPT_DIR/rcg-target/

export RCG_BUILD_LIBRTS=1
export SIMULATION_BUILD=1
rtcds build h1calcs --no-kernel-space
