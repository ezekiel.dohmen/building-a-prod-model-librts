# building-a-prod-model-librts
This repository shows how you can build a model with librts from an advligorts package install. Building production models is complex because a lot of environment setup is required. The `build.sh` script shows how this can be done. Sourcing the `userapps-user-env.sh` performs a lot of this setup and thus you are going to need a userapps checkout. 

## Building from the Repository 
All you should need to do, after installing the needed packages, is set the `USERAPPS_BASE_PATH` base path in the `build.sh` script and then run it.



## Building the docker container
The docker contianer is an attempt to show that the librts build works when installing from packages. 

#### Issue build command
```sh
$ docker build --tag librts-bullseye:unstable -f docker/Dockerfile_Bullseye_Unstable
```
#### Wait for build to complete, then find your userapps checkout
```sh
$ ls /opt/rtcds/trunk/
als  aos  asc  cal  cds  COPYING  COPYING-GPL-3  etc  fmcs  guardian  hpi  imc  ioo  isc  isi  lsc  oaf  omc  pem  psl  README  saf  sqz  sus  sys  tcs  ve
```

#### Run the container and mount in userapps
```sh
$ docker run --rm -it -v/opt/rtcds/trunk/:/opt/rtcds/trunk/ librts-bullseye:unstable
```

#### From inside the container build the production model with .mdls from ./models and .c files from userapps
```sh
controls@8f60e1e49c11:~/building-a-prod-model-librts$ ./build.sh
...
Building this model: h1calcs
Building model h1calcs ... Done.

Summary of 1 model builds
1 of 1 model builds were successful: h1calcs
```

#### Verify 
```sh
controls@311f3af326e0:~/building-a-prod-model-librts$ ls rcg-build/models/librts/python/pybind/
CMakeFiles  Makefile  cmake_install.cmake  h1calcs_pybind.cpython-39-x86_64-linux-gnu.so
```
